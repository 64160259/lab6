package com.panisa.week6;

public class RobotApp {
    private static int y;

    public static void main(String[] args) {
        Robot body = new Robot("Body", 'B', 1, 0);
        Robot petter = new Robot("Petter", 'P', 10, 10);
        body.print();
        body.right();
        body.print();
        petter.print();

        for(int i=0; i<=Robot.MAX_Y; y++) {
            for(int x=Robot.MIN_X; x<=Robot.MAX_X; x++) {
                if(body.getX() == x && body.getY() == y) {
                    System.out.print(body.getSymbol()); 
                } else if(petter.getX() == x && petter.getY() == y) {
                    System.out.print(petter.getSymbol());
                } else {
                    System.out.print("-");
                }
            }
            System.out.println();
        }
    }
}
