package com.panisa.week6;

public class App 
{
    public static void main( String[] args )
    {
        BookBank panisa = new BookBank("Panisa", 50.0);
        
        panisa.print();
        BookBank prayud = new BookBank("Prayudd",100000.0);
        prayud.print();
        prayud.withdraw(40000.0);
        prayud.print();

        panisa.deposit(40000.0);
        panisa.print();

        BookBank prawit = new BookBank("Prawit",10000.0);
        prawit.print();
        prawit.deposit(2);
        prawit.print();
    }
}
