package com.panisa.week6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest2 {
    @Test
    public void shouldRobotUpSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up();
        assertEquals(true, result);
        assertEquals(10, robot.getY());
    }

    @Test
    public void shouldRobotUpNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(5);
        assertEquals(true, result);
        assertEquals(6, robot.getY());
    }

    @Test
    public void shouldRobotUpNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(11);
        assertEquals(true, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRobotUpNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(12);
        assertEquals(false, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRobotUpNFail2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(32);
        assertEquals(false, result);
        assertEquals(0, robot.getY());
    }


    @Test
    public void shouldDownSuccess1() {
        Robot robot = new Robot("Robot", 'R', 0, 11);
        boolean result = robot.down(7);
        assertEquals(true, result);
        assertEquals(18, robot.getY());
    }


    @Test
    public void shouldDownSuccess2() {
        Robot robot = new Robot("Robot", 'R', 0, 7);
        boolean result = robot.down(7);
        assertEquals(true, result);
        assertEquals(14, robot.getY());
    }


    @Test
    public void shouldDownFail1() {
        Robot robot = new Robot("Robot", 'R', 0, 11);
        boolean result = robot.down(23);
        assertEquals(false, result);
        assertEquals(19, robot.getY());
    }


    @Test
    public void shouldDownFail2() {
        Robot robot = new Robot("Robot", 'R', 0, 14);
        boolean result = robot.down(34);
        assertEquals(false, result);
        assertEquals(19, robot.getY());
    }


    @Test
    public void shouldLeftSuccess1() {
        Robot robot = new Robot("Robot", 'R', 13, 11);
        boolean result = robot.left(6);
        assertEquals(true, result);
        assertEquals(7, robot.getX());
    }


    @Test
    public void shouldLeftSuccess2() {
        Robot robot = new Robot("Robot", 'R', 12, 11);
        boolean result = robot.left(12);
        assertEquals(true, result);
        assertEquals(0, robot.getX());
    }


    @Test
    public void shouldLeftFail1() {
        Robot robot = new Robot("Robot", 'R', 13, 11);
        boolean result = robot.left(17);
        assertEquals(false, result);
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldLeftFail2() {
        Robot robot = new Robot("Robot", 'R', 11, 11);
        boolean result = robot.left(11);
        assertEquals(false, result);
        assertEquals(0, robot.getX());
    }


    @Test
    public void shouldRightSuccess1() {
        Robot robot = new Robot("Robot", 'R', 4, 11);
        boolean result = robot.right(12);
        assertEquals(true, result);
        assertEquals(16, robot.getX());
    }


    @Test
    public void shouldRightSuccess2() {
        Robot robot = new Robot("Robot", 'R', 1, 11);
        boolean result = robot.right(11);
        assertEquals(true, result);
        assertEquals(12, robot.getX());
    }

    
    @Test
    public void shouldRightFail() {
        Robot robot = new Robot("Robot", 'R', 8, 11);
        boolean result = robot.right(13);
        assertEquals(false, result);
        assertEquals(19, robot.getX());
    }

}
